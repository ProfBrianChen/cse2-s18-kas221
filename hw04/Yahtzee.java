/*
Karthick Sivakumar
Professor Chen 
CSE 002 
*/
import java.util.Random;
import java.util.Scanner;
public class Yahtzee{
  public static void main (String args[]){
    //Recieve user inputs for random or custom dice roll
    Random rand = new Random();
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Welcome to the Yahtzee program");
    System.out.println("Would you like to roll a random number or input your own number?");
    System.out.print("If you would like to print your own number, please press '1', if you would like a rnadom number to be generated, please pres '0'");
    int randOrNot = myScanner.nextInt();
    String burner = myScanner.nextLine();
    int roll1 = 0;
    int roll2 = 0;
    int roll3 = 0;
    int roll4 = 0;
    int roll5 = 0;
    if (randOrNot == 1){
      System.out.print("Please enter a 5 digit number");
      String fiveDigitNumber = myScanner.nextLine();
      if (fiveDigitNumber.length() != 5){
        System.out.println("That is not a 5 digit number");
        System.exit(0);
      }
      else{
        for (int i = 0; i<fiveDigitNumber.length(); i++){
          if (fiveDigitNumber.charAt(i) < '1' || fiveDigitNumber.charAt(i) > '6'){
            System.out.println("Invalid input");
            break;
          }
          else{
            fiveDigitNumber.charAt(i);
              if (i == 0){
                roll1 = Character.getNumericValue(fiveDigitNumber.charAt(i));
                //System.out.println(roll1);
              }
              if (i == 1){
                roll2 = Character.getNumericValue(fiveDigitNumber.charAt(i));
                //System.out.println(roll2);
              }
              if (i == 2){
                roll3 = Character.getNumericValue(fiveDigitNumber.charAt(i));
                //System.out.println(roll3);
              }
              if (i == 3){
                roll4 = Character.getNumericValue(fiveDigitNumber.charAt(i));
                //System.out.println(roll4);
              }
              if (i == 4){
                roll5 = Character.getNumericValue(fiveDigitNumber.charAt(i));
                //System.out.println(roll5);

              }

          } 
            
        }
      }
      
    }
    else if (randOrNot == 0){
      System.out.println("Generating Random number....");
      roll1= 1+ (int)(rand.nextDouble()*6);
      roll2= 1+ (int)(rand.nextDouble()*6);
      roll3= 1+ (int)(rand.nextDouble()*6);
      roll4= 1+ (int)(rand.nextDouble()*6);
      roll5= 1+ (int)(rand.nextDouble()*6);
    } 
    int sumOnes = 0;
    int sumTwos = 0;
    int sumThrees = 0;
    int sumFours = 0;
    int sumFives = 0;
    int sumSixes = 0;
    int allOnes = 0;
    int allTwos = 0;
    int allThrees = 0;
    int allFours = 0;
    int allFives = 0;
    int allSixes = 0;
    //Upper Section
    //these 5 swtich statements recieve the sum of all the dice rolls
    switch (roll1){
      case 1:
        allOnes += 1;
      break;
      case 2:
        allTwos += 2;
      break;
      case 3:
        allThrees += 3;
      break;
      case 4:
        allFours += 4;
      break;
      case 5:
        allFives += 5;
      break;
      case 6:
        allSixes += 6;
      break;
    }
   switch (roll2){
      case 1:
        allOnes += 1;
      break;
      case 2:
        allTwos += 2;
      break;
      case 3:
        allThrees += 3;
      break;
      case 4:
        allFours += 4;
      break;
      case 5:
        allFives += 5;
      break;
      case 6:
        allSixes += 6;
      break;
   }
    switch (roll3){
      case 1:
        allOnes += 1;
      break;
      case 2:
        allTwos += 2;
      break;
      case 3:
        allThrees += 3;
      break;
      case 4:
        allFours += 4;
      break;
      case 5:
        allFives += 5;
      break;
      case 6:
        allSixes += 6;
      break;
    }
    switch (roll4){
      case 1:
        allOnes += 1;
      break;
      case 2:
        allTwos += 2;
      break;
      case 3:
        allThrees += 3;
      break;
      case 4:
        allFours += 4;
      break;
      case 5:
        allFives += 5;
      break;
      case 6:
        allSixes += 6;
      break;
    }
    switch (roll5){
      case 1:
        allOnes += 1;
      break;
      case 2:
        allTwos += 2;
      break;
      case 3:
        allThrees += 3;
      break;
      case 4:
        allFours += 4;
      break;
      case 5:
        allFives += 5;
      break;
      case 6:
        allSixes += 6;
      break;
    }
    //These 5 swtich statments recieve the number of each dice roll
    switch (roll1){
      case 1:
        sumOnes += 1;
      break;
      case 2:
        sumTwos += 1;
      break;
      case 3:
        sumThrees += 1;
      break;
      case 4:
        sumFours += 1;
      break;
      case 5:
        sumFives += 1;
      break;
      case 6:
        sumSixes += 1;
      break;
    }
   switch (roll2){
      case 1:
        sumOnes += 1;
      break;
      case 2:
        sumTwos += 1;
      break;
      case 3:
        sumThrees += 1;
      break;
      case 4:
        sumFours += 1;
      break;
      case 5:
        sumFives += 1;
      break;
      case 6:
        sumSixes += 1;
      break;
   }
    switch (roll3){
      case 1:
        sumOnes += 1;
      break;
      case 2:
        sumTwos += 1;
      break;
      case 3:
        sumThrees += 1;
      break;
      case 4:
        sumFours += 1;
      break;
      case 5:
        sumFives += 1;
      break;
      case 6:
        sumSixes += 1;
      break;
    }
    switch (roll4){
      case 1:
        sumOnes += 1;
      break;
      case 2:
        sumTwos += 1;
      break;
      case 3:
        sumThrees += 1;
      break;
      case 4:
        sumFours += 1;
      break;
      case 5:
        sumFives += 1;
      break;
      case 6:
        sumSixes += 1;
      break;
    }
    switch (roll5){
      case 1:
        sumOnes += 1;
      break;
      case 2:
        sumTwos += 1;
      break;
      case 3:
        sumThrees += 1;
      break;
      case 4:
        sumFours += 1;
      break;
      case 5:
        sumFives += 1;
      break;
      case 6:
        sumSixes += 1;
      break;
    }
    
    //Lower Section
    int twoOfAKind = 0;
    int threeOfAKind = 0;
    int fourOfAKind = 0;
    int fullHouse = 0;
    int smallStraight = 0;
    int longStraight = 0;
    int yahtzee = 0;
    int chance = 0;
    switch (sumOnes){
      case 2: 
        twoOfAKind = 2;
      case 3: 
        threeOfAKind = 3;
        break;
      case 4: 
        fourOfAKind = 4;
        break;
      case 5: 
        yahtzee = 50;
        break;
    }
    switch (sumTwos){
       case 2: 
        twoOfAKind = 4;
      case 3: 
        threeOfAKind = 6;
        break;
      case 4: 
        fourOfAKind = 8;
        break;
      case 5: 
        yahtzee = 50;
        break;
    }
    switch (sumThrees){
      case 2: 
        twoOfAKind = 6;
      case 3: 
        threeOfAKind = 9;
        break;
      case 4: 
        fourOfAKind = 12;
        break;
      case 5: 
        yahtzee = 50;
        break;
    }
    switch (sumFours){
      case 2: 
        twoOfAKind = 8;
      case 3: 
        threeOfAKind = 12;
        break;
      case 4: 
        fourOfAKind = 16;
        break;
      case 5: 
        yahtzee = 50;
        break;
    }
    switch (sumFives){
      case 2: 
        twoOfAKind = 10;
      case 3: 
        threeOfAKind = 15;
        break;
      case 4: 
        fourOfAKind = 20;
        break;
      case 5: 
        yahtzee = 50;
        break;
    }
    switch (sumSixes){
      case 2: 
        twoOfAKind = 12;
      case 3: 
        threeOfAKind = 18;
        break;
      case 4: 
        fourOfAKind = 24;
        break;
      case 5: 
        yahtzee = 50;
        break;
    }
   if(twoOfAKind != 0 && threeOfAKind != 0){
     fullHouse = 25;
   }   
   if((sumOnes > 0 && sumOnes <= 2) && (sumTwos > 0 && sumTwos <= 2) && (sumThrees > 0 && sumThrees <= 2) && (sumFours > 0 && sumFours <= 2)){
     smallStraight = 30;
   }
   else if((sumTwos > 0 && sumTwos <= 2) && (sumThrees > 0 && sumThrees <= 2) && (sumFours > 0 && sumFours <= 2) && (sumFives > 0 && sumFives <= 2)){
     smallStraight = 30;
   }
   else if((sumThrees > 0 && sumThrees <= 2) && (sumFours > 0 && sumFours <= 2) && (sumFives > 0 && sumFives <= 2) && (sumSixes > 0 && sumSixes <= 2)){
     smallStraight = 30;
   }
    
   if((sumOnes > 0 && sumOnes <= 2) && (sumTwos > 0 && sumTwos <= 2) && (sumThrees > 0 && sumThrees <= 2) && (sumFours > 0 && sumFours <= 2) && (sumFives > 0 && sumFives <= 2)){
     longStraight = 40;
   }
   else if((sumTwos > 0 && sumTwos <= 2) && (sumThrees > 0 && sumThrees <= 2) && (sumFours > 0 && sumFours <= 2) && (sumFives > 0 && sumFives <= 2) && (sumSixes > 0 && sumSixes <= 2)){
     longStraight = 40;
   }
   chance = roll1 + roll2 + roll3 + roll4 + roll5;   
  //final Calculations
   int upperSecTot = 0;
   upperSecTot = allOnes + allTwos + allThrees + allFours + allFives + allSixes;
   int upperSecTotBonus = upperSecTot;
   if (upperSecTot > 64){
    upperSecTotBonus = upperSecTot + 35;
   }
   int lowerTot = threeOfAKind + fourOfAKind + yahtzee + fullHouse + smallStraight + longStraight;
   int grandTotal = upperSecTotBonus + lowerTot;
   //final print statements
   System.out.println("The upper total without bonus is: " + upperSecTot + "\nThe upper total with bonus is: " + upperSecTotBonus + "\nthe lower total is: " + lowerTot + "\nthe grand total is: " + grandTotal);
    }
      
    }
