public class WelcomeClass
{
  
  
    public static void main(String[] args)
    {       
        System.out.println("     -----------");
        System.out.println("     | WELCOME |");
        System.out.println("     -----------");
        System.out.println();
        System.out.println("  ^  ^  ^  ^  ^  ^ ");
        System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
        System.out.println("<-K--A--S--2--2--1->"); 
        System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); 
        System.out.println("  v  v  v  v  v  v ");
        System.out.println();                   
        System.out.println("Hi I am Karthick, I was born in Minnesota and I love computers.");                   
    }
}