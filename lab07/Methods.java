import java.util.Random;
import java.util.Scanner;

public class Methods{
  public static void main(String[] args){
    Random randgen = new Random();
    int randomInt = 0;
    String adj = " ";
    String sn = " ";
    String pv = " ";
    String on = " ";
    String newsub = " ";
    for (int i = 0; i < 8; i++){
      randomInt = randgen.nextInt(10);
      adj = adjective(randomInt);
      sn = subnoun(randomInt);
      pv = pastverb(randomInt);
      on = obnoun(randomInt);
      switch (i){
      case 0: 
        System.out.print("The ");
        break;
      case 1:
        System.out.print(adj + " ");
        break;
      case 2:
        System.out.print(adj + " ");
        break;
      case 3:
        System.out.print(sn + " ");
        newsub = sn;
        break;
      case 4:
        System.out.print(pv + " ");
        break;
      case 5:
        System.out.print("the ");
        break;
      case 6:
        System.out.print(adj + " ");
        break;
      case 7:
        System.out.print(on + " ");
    }
     while (i == 7){
       Scanner scan = new Scanner(System.in);
       System.out.println("Would you like another sentence? Enter 1 for yes and 0 for no");
       int anothersentence = scan.nextInt();
       if (anothersentence == 1){
         i = 0;
       }
       break;
     }
  }
    for (int i = 0; i < 11; i++){
      randomInt = randgen.nextInt(10);
      adj = adjective(randomInt);
      sn = subnoun(randomInt);
      pv = pastverb(randomInt);
      on = obnoun(randomInt);
      switch (i){
      case 0: 
        System.out.print("This ");
        break;
      case 1:
        System.out.print(adj + " ");
        break;
      case 2:
        System.out.print(adj + " ");
        break;
      case 3:
        System.out.print(newsub + " ");
        break;
      case 4:
        System.out.print(pv + " ");
        break;
      case 5:
        System.out.print("the ");
        break;
      case 6:
        System.out.print(adj + " ");
        break;
      case 7:
        System.out.print(on + " ");
      case 8:
        System.out.print("and ");
        break;
      case 9:
        System.out.print(pv + " ");
        break;
      case 10:
        System.out.print(on + " ");
    }
  }
  }
  public static String adjective(int randint){
    switch (randint){
      case 1: 
        return "fast";
      case 2:
        return "slow";
      case 3:
        return "big";
      case 4:
        return "small";
      case 5:
        return "hard";
      case 6:
        return "soft";
      case 7:
        return "cold";
      case 8:
        return "warm";
      case 9:
        return "gross";
    }
    return " ";
  }
  public static String subnoun(int randint1){
    switch (randint1){
      case 1: 
        return "fox";
      case 2:
        return "cat";
      case 3:
        return "dog";
      case 4:
        return "boy";
      case 5:
        return "girl";
      case 6:
        return "man";
      case 7:
        return "woman";
      case 8:
        return "family";
      case 9:
        return "cousin";
    }
    return " ";
  }
  public static String pastverb(int randint2){
    switch (randint2){
      case 1: 
        return "went";
      case 2:
        return "sat";
      case 3:
        return "ran";
      case 4:
        return "licked";
      case 5:
        return "typed";
      case 6:
        return "drank";
      case 7:
        return "ate";
      case 8:
        return "killed";
      case 9:
        return "studied";
    }
    return " ";
  }
  public static String obnoun(int randint3){
    switch (randint3){
      case 1: 
        return "tree";
      case 2:
        return "grass";
      case 3:
        return "house";
      case 4:
        return "toy";
      case 5:
        return "garage";
      case 6:
        return "bike";
      case 7:
        return "car";
      case 8:
        return "truck";
      case 9:
        return "building";
    }
    return " ";
}
}