/*
Karthick Sivakumar
CSE002
Professor Brian Chen
HW03
The purpose of this program is to recieve and input of hurrican stats and do a calculation
This program tests our understanding of input statments and calculations
*/
import java.util.Scanner;
public class Convert {

  public static void main(String[] args) {
    Scanner myScanner = new Scanner(System.in);
    //Receiving inputs
    System.out.print("Enter the affected area in acres: ");//Recieves total area affected
    double affectedArea = myScanner.nextDouble();
    System.out.print("Enter the rainfall in the affected areas in inches: ");//Recieves how many inches of rainfall
    double rainfallInches = myScanner.nextDouble();
    //Calculations
    double gallonsOfWater = (affectedArea * rainfallInches) * 27154.2857; //This calculates the amount of water in gallons.
    double cubicMiles = gallonsOfWater / 1101117147428.5713; //converts gallons to cubic miles
    System.out.println("The amount of cubic miles of water in the affected area is: " + cubicMiles);
  }
}