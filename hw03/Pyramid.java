/*
Karthick Sivakumar
CSE002
Professor Brian Chen
HW03
The purpose of this program is to recieve and input of hurrican stats and do a calculation
This program tests our understanding of input statments and calculations
*/
import java.util.Scanner;
public class Pyramid{
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner(System.in);
    //User Inputs
    System.out.print("The square side of the pyramid is: ");//recieves side length of pyramid
    double squareSide = myScanner.nextDouble();
    System.out.print("The height of the pyramid is: ");//receives hieght of pyramid
    double pyramidHeight = myScanner.nextDouble();
    //Calculations
    double pyramidVolume = (squareSide * squareSide * pyramidHeight) / 3; //formula for a volume of a pyramid
    //Outputs
    System.out.println("The Volume of this pyramid is: " + pyramidVolume);//outputs volume of pyramid
  }//end of main method
}//end of class