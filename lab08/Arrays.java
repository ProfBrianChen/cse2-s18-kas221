import java.util.Scanner;
import java.util.Random;
public class Arrays
{
  public static void main(String[] args)
  {
    Random rand = new Random(); //initialize random
    Scanner scan = new Scanner(System.in); //initialize scanner
    int randnum1 = rand.nextInt(6) + 5; //create random number from 5-10
    String [] students = new String[randnum1]; //creates new string array with size being random number generated previously
    for (int i = 0; i < randnum1; i++)
    {
      System.out.println("Please input the name of " + randnum1 + " students.");
      students[i] = scan.nextLine();//takes user input and stores it in every slot of array student
    }
    int [] midtermGrades = new int[randnum1]; //initializes midterm grades array with size randnum1
    for (int j = 0; j < randnum1; j++)
    {
      int randnum2 = rand.nextInt(100);
      midtermGrades[j] = randnum2;
    }
    for (int k = 0; k < randnum1; k++){
     System.out.println("Student: " + students[k] + "     Midterm Grade:" + midtermGrades[k]);//final print statement prints all values
    }
  }
}