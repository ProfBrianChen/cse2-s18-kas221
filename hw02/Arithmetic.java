public class Arithmetic
{
  public static void main(String[] args)
  {
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    
    //cost per box of envelopes
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //calcualtions
    //total cost of each item
    double tcoPants; //Total cost of pants
    tcoPants = numPants * pantsPrice;
    double tcoShirts; //Total cost of shirts
    tcoShirts = numShirts * shirtPrice;
    double tcoBelt; //Total cost of belts
    tcoBelt = numBelts * beltCost;
    //sales tax charged for each item
    double stcPants; //sales tax charged for pants
    stcPants = tcoPants * paSalesTax;
    double stcShirts; //sales tax charged for shirts
    stcShirts = tcoShirts * paSalesTax;
    double stcBelt; //sales tax charged for belt
    stcBelt = tcoBelt * paSalesTax;
    //totals
    double tcop; //Total cost of purchase before tax
    tcop = tcoPants + tcoShirts + tcoBelt;
    double tst; //Total sales tax
    tst = stcPants + stcShirts + stcBelt;
    double tpp; //Total purchase price including tax
    tpp = tcop + tst;
    //Print out values
    System.out.printf("The total cost of the pants are: %.2f\n", tcoPants);
    System.out.printf("The total cost of the shirts are: %.2f\n", tcoShirts);
    System.out.printf("The total cost of the belt: %.2f\n", tcoBelt);
    System.out.printf("The total tax of the pants are: %.2f\n", stcPants);
    System.out.printf("The total tax of the shirts are: %.2f\n", stcShirts);
    System.out.printf("The total tax of the belt: %.2f\n", stcBelt);
    System.out.printf("The total cost of the purchase pre-tax is: %.2f\n", tcop);
    System.out.printf("The total of the tax is: %.2f\n", tst);
    System.out.printf("The total cost of the purchase including tax is: %.2f\n", tpp);
    
  }
    
}