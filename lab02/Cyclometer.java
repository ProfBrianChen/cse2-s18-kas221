//Karthick Sivakumar
//2/2/2018
//CSE002
//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480;  //This variable stores the seconds of the first trip
      int secsTrip2=3220;  //This variable stores the seconds of the second trip
      int countsTrip1=1561;  //This variable stores the counts of the first trip
      int countsTrip2=9037; //This variable stores the counts of the second trip
      // Intermediate variables
      // Theses variables store the conversions for distances as well as other
      // intermediate variables
      double wheelDiameter=27.0,  //This variable stores the Wheel diameter
  	  PI=3.14159, //This variable stores the value of pi
  	  feetPerMile=5280,  //This variable stores the conversion of feet to miles
  	  inchesPerFoot=12,   //This variable stores the conversion of inches to feet
  	  secondsPerMinute=60;  //This variable stores the conversion of seconds to minutes
	    double distanceTrip1, distanceTrip2,totalDistance;  //This initializes the three different distane variables
      //Print Statements that print out the time and the count for the two trips
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      //Distance Calculations
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	    distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
	    totalDistance=distanceTrip1+distanceTrip2;
      //Print out the output data.
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class
